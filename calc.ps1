# prep your commands first and then invoke them via powershell 
$command = 'cmd.exe /c calc.exe' 
$bytes = [System.Text.Encoding]::Unicode.GetBytes($command) 
$encodedCommand = [Convert]::ToBase64String($bytes) 
# once you have b64 string payload, execute it 
powershell.exe -encodedCommand $encodedCommand 
